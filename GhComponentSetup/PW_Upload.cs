﻿using System;
using System.IO;
using Grasshopper.Kernel;
using System.Net;
using System.Collections.Specialized;
using System.IO.Compression;
using GhComponentSetup;
using Newtonsoft.Json;


// your namespace
namespace ParallelWorks
{
    // your component class (inherits Grasshopper.Kernel.GH_Component)
    public class Upload : GH_Component
    {
        #region Constructor

        // default constructor (must be empty)
        public Upload() : base("ParallelWorks_Upload", "PW_Upload", "uploads job to Parallel.Works", "//", "//")
        {
        }

        // input parameters
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddBooleanParameter("start", "Start", "Start Upload", GH_ParamAccess.item, false);
            pManager.AddTextParameter("project_name", "Name", "Project Name", GH_ParamAccess.item);
            pManager.AddTextParameter("api_key", "API Key", "API Key", GH_ParamAccess.item);
            pManager.AddTextParameter("workspace_name", "Workspace Name", "Workspace Name", GH_ParamAccess.item);

            /* note:
             * input parameters without a default value (e.g. epw above) will require user input for solveinstance to run...
             * unless you to this:
             */
            //pManager[1].Optional = true;
        }

        // output parameters
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddTextParameter("output", "out", "your output parameter", GH_ParamAccess.item);

        }

        #endregion

        #region Properties

        // component guid
        public override Guid ComponentGuid
        {
            get { return new Guid("2304335e-ca62-4465-9219-c0568cdcc8ee"); }
        }

        // component icon
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // you can add your own image(s) or icon(s) to the project properties / resources tab
                return ParallelWorksComponent.Properties.Resources.PWupload;
            }
        }

        #endregion

        #region Methods

        // instance solver
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // retrieve inputs
            // ***************************************************************

            // input 0
            bool start = false;
            if (!DA.GetData(0, ref start))
            {
                // input collection failed, feel free to do something about it
            }
            
            // input 1
            string project_name = "";
            DA.GetData(1, ref project_name);
            if (!DA.GetData(1, ref project_name))
            {
                // input collection failed, feel free to do something about it
            }

            // input 2
            string api_key = "";
            DA.GetData(2, ref api_key);
            if (!DA.GetData(2, ref api_key))
            {
                // input collection failed, feel free to do something about it
            }

            // input 3
            string workspace_name = "";
            DA.GetData(3, ref workspace_name);
            if (!DA.GetData(3, ref workspace_name))
            {
                // input collection failed, feel free to do something about it
            }

            // do some stuff
            // ***************************************************************

            // wait for start instruction
            if (!start) { return; }

            // PW API COMMANDS

            string pw_api_url = "https://eval.parallel.works/api";

            // set the current working directory to GH_Data
            Directory.SetCurrentDirectory(@"C:\Users\Administrator\Documents\DIVA\GH_Data");

            string zipPath = project_name+".zip";
            // delete the zip file if it already exists
            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }

            string workspace_id = GetHistoryName(pw_api_url, api_key, workspace_name);

            // find and copy the weather file to the zipPath (currently this is specific to Glare study)
            CopyWeatherFile(project_name);

            ZipFile.CreateFromDirectory(project_name, zipPath, CompressionLevel.NoCompression, true,new LinuxEncoder());

            // PW SIMPLE UPLOADING
            NameValueCollection payload = new NameValueCollection();
            NameValueCollection files = new NameValueCollection();

            payload.Add("key", api_key);
            payload.Add("tool_id", "upload1");
            payload.Add("workspace_id", workspace_id);

            string fileLocation = zipPath;
            files.Add("files_0|file_data", fileLocation);

            // get the file ids as the response values
            string upload = send_pw_request(pw_api_url+"/tools", payload, files);

            // poll the upload state until file is ok
            dynamic response = JsonConvert.DeserializeObject(upload);
            string dataset_id = response.outputs[0].id;
            System.Timers.Timer timer = new System.Timers.Timer(1000);
            while (true)
            {
                timer.Start();
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url+"/datasets/" + dataset_id));
                string state = progress.state;
                DA.SetData(0, state);
                if (state == "ok")
                {
                    break;
                }
            }

            DA.SetData(0, dataset_id);
            
            return;
        }

        // other methods...

        #endregion


        #region PW HELPER FUNCTIONS

        private static string GetHistoryName(string pw_api_url, string api_key, string history_name)
        {
            // this function gets the history id from the users history name
            WebClient client = new WebClient();
            dynamic response = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/histories?key=" + api_key));
            string history_id = null;
            foreach (dynamic history in response)
            {
                if (history.name == history_name)
                {
                    history_id = history.id;
                }
            }
            return history_id;
        }

        private static string send_pw_request(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }

        // CURRENTLY VERY SPECIFIC TO GLARE - MAKE THIS BETTER
        private static void CopyWeatherFile(string project_name)
        {
            int counter = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(project_name + "/Glare/" + project_name + ".sh");
            while ((line = file.ReadLine()) != null)
            {
                if (line.Contains("C:\\"))
                {
                    // weather file needed in all but DaylightFactor analysis
                    string filename = Path.GetFileName(line);
                    if (Directory.Exists(project_name+"/Glare"))
                    {
                        File.Copy(line, project_name + "/Glare/" + filename, true);
                    }
                    if (Directory.Exists(project_name + "/AnnualDaylight"))
                    {
                        File.Copy(line, project_name + "/AnnualDaylight/" + filename, true);
                    }
                    if (Directory.Exists(project_name + "/Illuminance"))
                    {
                        File.Copy(line, project_name + "/Illuminance/" + filename, true);
                    }
                    if (Directory.Exists(project_name + "/RadImage"))
                    {
                        File.Copy(line, project_name + "/RadImage/" + filename, true);
                    }
                    if (Directory.Exists(project_name + "/RadMap"))
                    {
                        File.Copy(line, project_name + "/RadMap/" + filename, true);
                    }
                    if (Directory.Exists(project_name + "/Render"))
                    {
                        File.Copy(line, project_name + "/Render/" + filename, true);
                    }

                }
                counter++;
            }
            file.Close();
        }

        #endregion


    }



}

