﻿using System;
using System.IO;
using System.IO.Compression;
using Grasshopper.Kernel;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Threading.Tasks;
using GhComponentSetup;
using System.Timers;

namespace ParallelWorks
{
    public class UploadExecute : GH_Component
    {
        #region Constructor

        public string defaultDirectory = String.Format(@"C:\Users\{0}\Documents\DIVA\GH_Data", Environment.UserName);
        //Directory.SetCurrentDirectory(String.Format(@"C:\Users\{0}\Documents\DIVA\GH_Data", "Custom User"));

        // default constructor (must be empty)
        public UploadExecute() : base("ParallelWorks_UploadExecute", "PW Upload & Execute", "Upload files and execute jobs on Parallel.Works", "//", "//")
        {
        }

        // input parameters
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddBooleanParameter("start", "Start", "Start Running Workflow", GH_ParamAccess.item, false);
            pManager.AddTextParameter("name", "Name", "Name", GH_ParamAccess.item);
            pManager.AddTextParameter("api_key", "API Key", "API Key", GH_ParamAccess.item);
            pManager.AddTextParameter("workspace_name", "Workspace", "Workspace Name", GH_ParamAccess.item,"diva_runner_v2");
            pManager.AddTextParameter("workflow_name", "Workflow", "Workflow Name", GH_ParamAccess.item, "diva_runner_v2");
            pManager.AddTextParameter("base_dir", "Base Dir", "Base Directory", GH_ParamAccess.item, defaultDirectory);
            pManager.AddTextParameter("update", "Update", "waiting for job", GH_ParamAccess.item);

            pManager[6].Optional = true;

        }

        // output parameters
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddTextParameter("output", "Log", "run log", GH_ParamAccess.item);

        }

        #endregion

        #region Properties

        // component guid
        public override Guid ComponentGuid
        {
            get { return new Guid("9DEB8C2A-CD3F-42F7-82DD-75494C345026"); }
        }

        // component icon
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // you can add your own image(s) or icon(s) to the project properties / resources tab
                return ParallelWorksComponent.Properties.Resources.PWupload;
            }
        }

        #endregion

        #region Methods

        // instance solver
        protected override void SolveInstance(IGH_DataAccess DA)
        {

            // retrieve inputs
            // ***************************************************************

            // input 0
            bool start = false;
            DA.GetData(0, ref start);
            if (!DA.GetData(0, ref start))
            {
                // input collection failed, feel free to do something about it
            }

            // input 1
            string name = "";
            DA.GetData(1, ref name);
            if (!DA.GetData(1, ref name))
            {
                // input collection failed, feel free to do something about it
            }

            string project_name = name;

            // input 2
            string api_key = "";
            DA.GetData(2, ref api_key);
            if (!DA.GetData(2, ref api_key))
            {
                
            }

            // input 3
            string workspace_name = "";
            DA.GetData(3, ref workspace_name);
            if (!DA.GetData(3, ref workspace_name))
            {
                 
            }

            // input 4
            string tool_id = "";
            DA.GetData(4, ref tool_id);
            if (!DA.GetData(4, ref tool_id))
            {
                
            }

            string defaultDir = "";
            DA.GetData(5, ref defaultDir);
            if (!DA.GetData(5, ref defaultDir))
            {

            }

            string input = "";
            DA.GetData(6, ref input);
            if (!DA.GetData(6, ref input))
            {
                
            }

            string status = this.Params.Input[6].Description;

            // ***************************************************************

            try
            {
                // PW API COMMANDS

                string pw_api_url = "https://eval.parallel.works/api";

                if (status == "waiting for job" && start)
                {

                    // UPLOAD PW FILES
                    DA.SetData(0, "Uploading to Parallel Works");

                    // set the current working directory to GH_Data - MAKE THIS GENERAL
                    Directory.SetCurrentDirectory(defaultDir);

                    string zipPath = "";
                    if (project_name.Contains(@"/"))
                    {
                        zipPath = project_name.Substring(0, project_name.LastIndexOf((@"/"))) + ".zip";
                    }
                    else if (project_name.Contains(@"\"))
                    {
                        zipPath = project_name.Substring(0, project_name.LastIndexOf((@"\"))) + ".zip";
                    }
                    else
                    {
                        zipPath = project_name + ".zip";
                    }
                    
                    // delete the zip file if it already exists
                    if (File.Exists(zipPath))
                    {
                       File.Delete(zipPath);
                    }

                    // TODO - if history does not exist, create it and return the new id
                    try
                    {
                        string workspace_id = GetWorkspaceName(pw_api_url, api_key, workspace_name);

                        // find and copy the weather file to the zipPath
                        CopyWeatherFile(project_name);

                        ZipFile.CreateFromDirectory(project_name, zipPath, CompressionLevel.Fastest, true, new LinuxEncoder());

                        // PW SIMPLE UPLOADING
                        NameValueCollection payload = new NameValueCollection();
                        NameValueCollection files = new NameValueCollection();

                        payload.Add("key", api_key);
                        payload.Add("tool_id", "upload1");
                        payload.Add("workspace_id", workspace_id);

                        string fileLocation = zipPath;
                        files.Add("files_0|file_data", fileLocation);

                        // get the file ids as the response values
                        string upload = send_pw_request(pw_api_url + "/tools", payload, files);

                        dynamic response = JsonConvert.DeserializeObject(upload);
                        string dataset_id = response.outputs[0].id;

                        DA.SetData(0, "Upload Started: " + dataset_id + " - Polling the Upload for Completion.");
                        this.Params.Input[6].Description = "poll upload:" + dataset_id;

                    }
                    catch (SystemException Ex)
                    {

                        DA.SetData(0, Ex.ToString());

                        //DA.SetData(0, "User does not have access to this workspace... Waiting for New Parallel Works Job");
                        this.Params.Input[6].Description = "waiting for job";
                    }

                }
                else if (status.Contains("poll upload:"))
                {

                    string dataset_id = status.Replace("poll upload:", "");

                    // check the status of the file
                    WebClient client = new WebClient();
                    dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/datasets/" + dataset_id));
                    string state = progress.state;

                    // if status is ok, change state of status to execute
                    if (state == "ok")
                    {
                        DA.SetData(0, "Upload Complete - Starting Job.");
                        this.Params.Input[6].Description = "start job:" + dataset_id;
                    }
                    else
                    {
                        DA.SetData(0, "Upload: " + dataset_id + " - State: " + state.ToUpper());
                        this.Params.Input[6].Description = "poll upload:" + dataset_id;
                    }

                }
                else if (status.Contains("start job:"))
                {
                    string file_id = status.Replace("start job:", "");

                    NameValueCollection payload = new NameValueCollection();
                    NameValueCollection files = new NameValueCollection();

                    string inputs = "{\"divazip\":{\"values\":[{\"src\":\"hda\",\"id\":\"" + file_id + "\"}]},\"animate\":\"" + input + "\"}";

                    // get the workspace id from the provided workspace name
                    string workspace_id = GetWorkspaceName(pw_api_url, api_key, workspace_name);
                    // get the valid workflow name based on the API key
                    string valid_tool_name = GetWorkflowName(pw_api_url, api_key, tool_id);

                    if (valid_tool_name=="not valid")
                    {
                        DA.SetData(0, "User does not have access to this workflow... Waiting for New Parallel Works Job");
                        this.Params.Input[6].Description = "waiting for job";
                    }
                    else
                    {
                        
                        payload.Add("key", api_key);
                        payload.Add("tool_id", valid_tool_name);
                        payload.Add("workspace_id", workspace_id);
                        payload.Add("inputs", inputs);

                        string execute = send_pw_request(pw_api_url + "/tools", payload, files);
                        dynamic response = JsonConvert.DeserializeObject(execute);
                        string job_id = response.jobs[0].id;

                        DA.SetData(0, "Job Started: " + job_id + " - Polling the Job for Completion.");
                        this.Params.Input[6].Description = "poll job:" + job_id;

                    }

                }
                else if (status.Contains("poll job:"))
                {
                    string job_id = status.Replace("poll job:", "");

                    WebClient client = new WebClient();
                    dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/jobs/" + job_id + "?key=" + api_key));
                    string state = progress.state;

                    if (state == "ok")
                    {
                        string output_id = progress.outputs.results.id;
                        DA.SetData(0, "Job Complete - Downloading Results.");
                        this.Params.Input[6].Description = "download results:" + output_id;
                    }
                    else if (state == "deleted" || state == "error")
                    {
                        DA.SetData(0, "Error in Last Simulation. Waiting for New Parllel Works Job");
                        this.Params.Input[6].Description = "waiting for job";
                    }
                    else
                    {
                        DA.SetData(0, "Job: " + job_id + " - State: " + state.ToUpper());
                        this.Params.Input[6].Description = "poll job:" + job_id;
                    }

                }
                else if (status.Contains("download results:"))
                {

                    string output_id = status.Replace("download results:", "");

                    WebClient client1 = new WebClient();
                    dynamic output = JsonConvert.DeserializeObject(client1.DownloadString(pw_api_url + "/datasets/" + output_id + "?key=" + api_key));
                    string output_url = pw_api_url.Replace("/api", "") + output.download_url;

                    DA.SetData(0, "Downloading Result: " + output.download + output_url);

                    string targetDirectory = String.Format(@"{0}\{1}", defaultDir, project_name);
                    Directory.SetCurrentDirectory(targetDirectory);
                    WebClient client2 = new WebClient();

                    if (File.Exists(targetDirectory + "/results.zip"))
                    {
                        File.Delete(targetDirectory + "/results.zip");
                    }

                    if (Directory.Exists(targetDirectory + "/tmp"))
                    {
                        Directory.Delete(targetDirectory + "/tmp", true);
                    }
                    client2.DownloadFile(output_url, targetDirectory + "/results.zip");

                    // unzip the result file
                    ZipFile.ExtractToDirectory(targetDirectory + "/results.zip", targetDirectory + "/tmp");

                    CopyAllContents(targetDirectory + "/tmp", targetDirectory);

                    // cleanup
                    Directory.Delete(targetDirectory + "/tmp", true);
                    File.Delete(targetDirectory + "/results.zip");

                    DA.SetData(0, "Waiting for New Parllel Works Job");
                    this.Params.Input[6].Description = "waiting for job";

                }
                else
                {
                    DA.SetData(0, "Waiting for New Parllel Works Job");
                    this.Params.Input[6].Description = "waiting for job";
                }

                // IMPLEMENT THE UPLOAD AS NON-BLOCKING REPEAT SIMILAR TO EXECUTION
                GH_Document doc = OnPingDocument();
                if (doc != null && this.Params.Input[6].Description != "waiting for job")
                {
                    doc.ScheduleSolution(1000, ScheduleCallback);
                }
                else
                {
                }

            }
            catch (SystemException Ex)
            {
                DA.SetData(0, "Error with last job... Waiting for New Parallel Works Job");
                //DA.SetData(0,Ex.ToString());
                this.Params.Input[6].Description = "waiting for job";
            }

            // done
            return;

        }

        // other methods...

        #endregion


        #region PW HELPER FUNCTIONS

        private void ScheduleCallback(GH_Document document)
        {
            //ExpireDownStreamObjects();
            ExpireSolution(false);
            //document.NewSolution(false);
        }

        private static void CopyAllContents(string SourcePath, string DestinationPath)
        {
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));
            }
               

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath),true);

        }

        private static string GetWorkflowName(string pw_api_url, string api_key, string workflow_name)
        {
            // this function gets the valid workflow name based on the API key
            WebClient client = new WebClient();
            string name = client.DownloadString(pw_api_url.Replace("/api","") + "/workflow_name/" + workflow_name + "?key=" + api_key);
            return name;
        }

        private static string GetWorkspaceName(string pw_api_url, string api_key, string history_name)
        {
            // this function gets the history id from the users history name
            WebClient client = new WebClient();
            dynamic response = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/histories?key=" + api_key));
            string history_id = null;
            foreach (dynamic history in response)
            {
                if (history.name == history_name)
                {
                    history_id = history.id;
                }
            }
            return history_id;
        }

        private static string send_pw_request(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }

        private static string WaitForCompletion(string pw_api_url,string api_key,string job_id)
        {
            System.Timers.Timer timer = new System.Timers.Timer(3000);
            while (true)
            {
                timer.Start();
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/jobs/" + job_id + "?key=" + api_key));
                string state = progress.state;
                if (state == "ok")
                {
                    break;
                }
            }
            return "job complete";
        }

        public static void ProcessDirectory(string targetDirectory,string project_name)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName,project_name);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                ProcessDirectory(subdirectory,project_name);
        }

        public static void ProcessFile(string path,string project_name)
        {
            Console.WriteLine("Processed file '{0}'.", path);
            System.IO.StreamReader file;
            string line;
            if (path.Contains(".sh"))
            {
                file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Contains("C:\\"))
                    {
                        line = line.Replace("#", "");
                        string filename = Path.GetFileName(line);
                        File.Copy(line, project_name + "/" + filename, true);
                    }
                }
                file.Close();
            }
        }

        private static void CopyWeatherFile(string project_name)
        {

            // look through the files and find any needed weather files
            ProcessDirectory(project_name,project_name);
           
        }

                #endregion


            }
}
