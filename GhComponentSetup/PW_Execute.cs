﻿using System;
using System.IO;
using Grasshopper.Kernel;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO.Compression;

namespace ParallelWorks
{
    public class Execute : GH_Component
    {
        #region Constructor

        // default constructor (must be empty)
        public Execute() : base("ParallelWorks_Execute", "PW_Execute", "Execute jobs on Parallel.Works", "//", "//")
        {
        }

        // input parameters
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddBooleanParameter("start", "Start", "Start Running Workflow", GH_ParamAccess.item, false);
            pManager.AddTextParameter("name", "Name", "Name", GH_ParamAccess.item);
            pManager.AddTextParameter("api_key", "API Key", "API Key", GH_ParamAccess.item);
            pManager.AddTextParameter("workspace_name", "Workspace Name", "Workspace ID", GH_ParamAccess.item);
            pManager.AddTextParameter("workflow_id", "Workflow ID", "Workflow ID", GH_ParamAccess.item);
            pManager.AddTextParameter("file_id", "File ID", "File ID", GH_ParamAccess.item);
            pManager.AddTextParameter("status", "Status", "waiting for job", GH_ParamAccess.item);

            pManager[6].Optional = true;

        }

        // output parameters
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add param (name, nickname (appears on component), description (appears on mouse-over), access type, default value)
            pManager.AddTextParameter("output", "out", "your output parameter", GH_ParamAccess.item);
            pManager.AddTextParameter("run_mode", "run mode", "run mode", GH_ParamAccess.item);

        }

        #endregion

        #region Properties

        // component guid
        public override Guid ComponentGuid
        {
            get { return new Guid("5830EC9E-C737-4DD5-96A9-5B748EBE9CC9"); }
        }

        // component icon
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // you can add your own image(s) or icon(s) to the project properties / resources tab
                return ParallelWorksComponent.Properties.Resources.PWupload;
            }
        }

        #endregion

        #region Methods

        // instance solver
        protected override void SolveInstance(IGH_DataAccess DA)
        {

            // retrieve inputs
            // ***************************************************************

            // input 0
            bool start = false;
            if (!DA.GetData(0, ref start))
            {
                // input collection failed, feel free to do something about it
            }

            // input 1
            string project_name = "";
            DA.GetData(1, ref project_name);
            if (!DA.GetData(1, ref project_name))
            {
                // input collection failed, feel free to do something about it
            }

            // input 2
            string api_key = "";
            DA.GetData(2, ref api_key);
            if (!DA.GetData(2, ref api_key))
            {
                // input collection failed, feel free to do something about it
            }

            // input 3
            string workspace_name = "";
            DA.GetData(3, ref workspace_name);
            if (!DA.GetData(3, ref workspace_name))
            {
                // input collection failed, feel free to do something about it
            }

            // input 4
            string tool_id = "";
            DA.GetData(4, ref tool_id);
            if (!DA.GetData(4, ref tool_id))
            {
                // input collection failed, feel free to do something about it
            }

            // input 5
            string file_id = "";
            DA.GetData(5, ref file_id);
            if (!DA.GetData(5, ref file_id))
            {
                // input collection failed, feel free to do something about it
            }

            string status = this.Params.Input[6].Description;

            // do some stuff
            // ***************************************************************

            // wait for start instruction

            if (!start) {

                DA.SetData(1, "Read Only");

                return;

            }

            // PW API COMMANDS

            // THIS LAUNCHES A PW TOOL

            string pw_api_url = "https://eval.parallel.works/api";

            DA.SetData(0, "Launching Parallel Works Job");

            DA.SetData(1, "Write Only");

            string workspace_id = GetHistoryName(pw_api_url, api_key, workspace_name);

            NameValueCollection payload = new NameValueCollection();
            NameValueCollection files = new NameValueCollection();

            string inputs = "{\"divazip\":{\"values\":[{\"src\":\"hda\",\"id\":\"" + file_id + "\"}]}}";

            payload.Add("key", api_key);

            // TODO - revise the tool_id to read a bit more friendly / statically
            payload.Add("tool_id", tool_id);
            payload.Add("workspace_id", workspace_id);
            payload.Add("inputs", inputs);

            if (status == "waiting for job")
            {
                string execute = send_pw_request(pw_api_url+"/tools", payload, files);
                dynamic response = JsonConvert.DeserializeObject(execute);
                string job_id = response.jobs[0].id;
                this.Params.Input[6].Description = job_id;
            }
            else if (status == "ok")
            {
                DA.SetData(0, "Waiting for New Job");
                this.Params.Input[6].Description = "waiting for job";
            }
            else
            {
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url+"/jobs/" + status + "?key=" + api_key));
                string state = progress.state;
                DA.SetData(0, "Job Status: "+state);
                if (state == "ok")
                {
                    DA.SetData(0, "Parallel Works Job Finished");

                    string output_id = progress.outputs.results.id;

                    // get the output datasets download url
                    WebClient client1 = new WebClient();
                    dynamic output = JsonConvert.DeserializeObject(client1.DownloadString(pw_api_url + "/datasets/" + output_id + "?key=" + api_key));
                    string output_url = "https://eval.parallel.works" + output.download_url;

                    DA.SetData(0, output_url);

                    // download the result file and unzip to targetDirectory
                    string targetDirectory = @"C:\Users\Administrator\Documents\DIVA\GH_Data\"+project_name;
                    Directory.SetCurrentDirectory(targetDirectory);
                    WebClient client2 = new WebClient();

                    if (File.Exists(targetDirectory + "/results.zip"))
                    {
                        File.Delete(targetDirectory + "/results.zip");
                    }

                    if (Directory.Exists(targetDirectory + "/tmp"))
                    {
                        Directory.Delete(targetDirectory + "/tmp",true);
                    }
                    client2.DownloadFile(output_url,targetDirectory+"/results.zip");

                    // unzip the result file
                    ZipFile.ExtractToDirectory(targetDirectory+"/results.zip", targetDirectory+"/tmp");

                    CopyAllContents(targetDirectory+"/tmp",targetDirectory);

                    // cleanup
                    Directory.Delete(targetDirectory + "/tmp", true);
                    File.Delete(targetDirectory + "/results.zip");
                    
                    this.Params.Input[6].Description = "waiting for job";

                    DA.SetData(1, "Read Only");
                }
            }

            GH_Document doc = OnPingDocument();
             
            if (doc != null && this.Params.Input[6].Description != "waiting for job")
                doc.ScheduleSolution(1000, ScheduleCallback);

            // done
            return;
        }

        // other methods...

        #endregion


        #region PW HELPER FUNCTIONS

        private static void CopyAllContents(string SourcePath, string DestinationPath)
        {
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));
            }
               

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath),true);

        }

        private static string GetHistoryName(string pw_api_url, string api_key, string history_name)
        {
            // this function gets the history id from the users history name
            WebClient client = new WebClient();
            dynamic response = JsonConvert.DeserializeObject(client.DownloadString(pw_api_url + "/histories?key=" + api_key));
            string history_id = null;
            foreach (dynamic history in response)
            {
                if (history.name == history_name)
                {
                    history_id = history.id;
                }
            }
            return history_id;
        }

        private void ScheduleCallback(GH_Document document)
        {
            ExpireSolution(false);
        }

        private static string send_pw_request(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }

        private static string WaitForCompletion(string api_key,string job_id)
        {
            System.Timers.Timer timer = new System.Timers.Timer(3000);
            while (true)
            {
                timer.Start();
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString("https://eval.parallel.works/api/jobs/" + job_id + "?key=" + api_key));
                string state = progress.state;
                if (state == "ok")
                {
                    break;
                }
            }
            return "job complete";
        }

        #endregion


    }
}
